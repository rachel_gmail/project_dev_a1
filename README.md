# Project_dev_A1

Assignment 1 repo

By Rachel Hague; May 28, 2019

- This C# code will return the depth-first-search (DFS) routes between two given nodes in a graph, given an input file with a list of paths. 
- To use it, download the sample input file "input.txt" and save it someone you can remember. 
=======
- To use it: 
	1. download the sample input file "input.txt" and save it somewhere you can remember. 
	2. Then run the software, 
	3. click on "Load Input File" and navigate to "input.txt"
	4. The values will appear in the list box on the left. Click "Process Input Values" to see the results


